![ordering](../images/modernx/20.ordering.png)

This option allows to change the order of the links by drag and drop items to the desired position. If the option is toggled off, the order of the links goes back to its original state.

<p class="alert alert-warning">
The ordering also works with old lists that use the field "Position" as sort criteria. When the Drag and Drop is enabled, the field position will be ignored. This new version doesn't write to the field "Position", so if you intend to use this field as sort criteria, just disable drag and drop and edit the field Position on the SharePoint native list view.
</p>