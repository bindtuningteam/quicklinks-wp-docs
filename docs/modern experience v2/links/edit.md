Go to edit mode, click on the web part to show edit controls. Then, hover on the desired link, click on the edit button, edit the link on the form and click save.

![edit_link](../../images/modernx/27.edit_link_button.png)

![edit_link](../../images/modernx/25.edit_link.png)