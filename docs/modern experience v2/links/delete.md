Go to edit mode, click on the web part to show edit controls. Then, hover on the desired link and click on the delete button and confirm.

![delete_link](../../images/modernx/26.delete_link.png)