Go to edit mode, click on the web part to show edit controls. Then, click on Add button

![add_link](../../images/modernx/22.add_link.png)

</br>

If the web part is connected o a list, just fill the link's information on the form, then click save

![add_link_form](../../images/modernx/23.add_link_form.png)

</br>

If the links source is "Save to this web part", then just get the link using the following data sources

- **Recent documents**
- **Stock images**
- **One drive**
- **Site collection**
- **Upload a file**
- **From a link**

![file_picker](../../images/modernx/24.file_picker.png)

</br>

If adding a link from an URL, the web part will automatically fill some useful information, like, the Title, Description and Image. This can be edited later.

![edit_link](../../images/modernx/25.edit_link.png)



