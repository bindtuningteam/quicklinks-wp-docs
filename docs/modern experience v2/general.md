![options](../images/modernx/04.property_pane.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Links Source](./datasource)
- [Layout](./layout)
- [Audience Targeting](./audiences)
- [Ordering](./ordering)
- [Theme](./theme)
- [Alerts](./alerts)