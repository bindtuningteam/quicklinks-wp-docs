1. Open the page where you want to add the web part and click on the **Edit** button;
    
    ![edit-page](../images/modernx/01.edit.modernx.png)
     
2. Click on the **[+]** button to add a new web part or section to your page;
3. On the web part panel, search for **Quick Links** to filter the web parts. Click on the **Quick Links** as shown at right;
    
    ![add_links](../images/modernx/02.add_links_mx.png)

Now the only thing left to do is to setup the rest of the **[Web Part Properties](./general.md)**.