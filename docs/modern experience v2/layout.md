Choose one of the 8 available layouts

![layout_options](../images/modernx/14.layout_options.png)

___
### Card color

On some layouts is possible to choose a background color for the card

![palette_button](../images/modernx/17.palette_button.png) | ![free_picker_button](../images/modernx/18.free_picker_button.png)
------------- | -------------
![color_picker](../images/modernx/15.color_picker.png)  | ![color_picker2](../images/modernx/16.color_picker2.png)

The options are:

- **BindTuning Theme** - Choose a color from the BindTuning Theme palette, if a theme is applied.
- **Change the look** - Choose a color from the Change the look palette.
- **Free picker** - Choose any other color.

<p class="alert alert-info">
All selected colors will result in a lighter color, since it will automatically set an opacity of 15%
</p>

___
### Columns

Select the desired number of columns for the grid layout. 

<p class="alert alert-info">
If the selected number of columns doesn't fit on the web part section, the number will be reduced to the best fit.
</p>
