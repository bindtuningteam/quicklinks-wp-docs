![Theme](../images/modernx/21.theme_options.png)

- **Apply theme color on title** - Displays the web part title with the theme's primary color on background.
- **Apply theme color on content** - Applies the theme's color on some elements of the web part's content .
- **Apply theme fonts** - Inherit the fonts of a BindTuning theme, if exists.