1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Links** icon;
3. The list of Links will appear. Click the **pencil** icon to edit the Links;
4. You can check what you can edit in each section on the [Link Settings](../../global/link); 

	![edit-map.gif](../../images/modern/03.edit_link.gif)

5. Done editing? Click on **Save Changes** to save your settings.