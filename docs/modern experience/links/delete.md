1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Links** icon;
3. The list of Links will appear. Click the **trash** icon to delete the Link;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Link will be removed.

![delete_link](../../images/modern/02.delete_link.gif)