1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Link item**;

	![add_link](../../images/modern/01.add_link.gif)

3. Fill out the form that pops up. You can check out what you need to do in each setting in the [Link Settings](../../global/link)

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more alerts with similar configuration. 