![list_links](../images/modern/09.list_links.png)

Paste the URL of the SharePoint list you created on step above.

Without the **List URL**, the web part Links will not be visible.

<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home/Lists/BTLinksList</b>, you should use something like <b>sites/Home/Lists/BTLinksList</b>.</p>

You can create a BindTuning Links list using the panel, for more information about this, visit the [next link](./createlist).