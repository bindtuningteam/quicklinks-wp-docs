![09.wpapperance.png](../images/msteams/appearance_text.png)

### Right to Left

Using this option will change the web part's text orientation to go from right to left. 
The forms will not be affected.