1. On the Tab click on the dropdown and then settings. If is the first time adding the **Quick Links**, you can skip this process. 
2. On The panel select the **Create New BT Links List**
3. Type the name of the list and hit **Create the List**;

    ![06.options](../images/modern/08.createlist.png)
    
4. The list will be created on your current Team Site and connected with the Web Part;