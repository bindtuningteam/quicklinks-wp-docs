1. Open the Team on **Teams panel** that you intend to remove the Web Part; 
2. Click on the **Settings** button.

	![setting_edit.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Links** icon;

	![Manage](../../images/msteams/manage_link.png)

4. The list of quick links will appear. Click the **trash can** icon to delete the link that you want to remove;

	![Delete](../../images/msteams/delete_link.png)

5. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the link will be removed.