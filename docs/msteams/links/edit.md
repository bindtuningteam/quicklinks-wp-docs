1. Open the Team on **Teams panel** that you intend to edit the Web Part; 
2. Click on the **Settings** button.

	![setting_edit.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Links** icon;

	![Manage](../../images/msteams/manage_link.png)

4. Change the <a href="../../global/link">Links Settings</a> as necessary to re-configure the link.

	![edit_link](../../images/msteams/edit_link.png)

5. Done editing? Click on the **Preview** button if you want to see how everything looks on the page or on **Save Changes**.