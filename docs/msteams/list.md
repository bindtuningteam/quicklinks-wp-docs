If you have already configured a **BindTuning Quick Links** list that you intend to display, you need to paste the **list URL** that you have copied when you finished to create the list. This list will be stored on a SharePoint site.

![list_links](../images/modern/09.list_links.png)


<p class="alert alert-info">We recommend you use relative paths in this field. So instead of using a URL like  https://company.sharepoint.com/sites/Home/Lists/BTLinksList you should use something like <b>/sites/Home/Lists/BTLinksList</b>. This will ensure that the web part will work regardless of how you’re accessing the site. </p>

You can create a BindTuning Quick Links list using the panel, for more information about this, visit the [next link](./createlist).