![linksettings](../images/classic/08.linksettings.png)

### Order

Set the order in which links appears. Links with lower values appear first and links with higher values appear last.
Links with the same value are ordered based on date of creation (older links first).

_____
### Title

This is the display text for the link. If no value is provided, link URL will be used in its place.

______
### URL

This is destination URL for the link. You can also set the target on click, open in the same window or open in a new window.

___
### Image or Icon

This setting allows you to set an **image** or **icon** to appear before the link display text (title). When selecting an image, simply type the image link in the text box or click on **Browse** to search on your SharePoint site an [image to use](./image). For an icon, select the corresponding option from the dropdown and choose the icon from the provided list.

____
### Target Users

This options allows you to select which users will be able to see the link. If no users or groups are set, the link will
be visible for everyone with access to the site collection where the web part is located.

<p class="alert alert-success">If you plan to use Active Directory groups to target your content, please check <a href="https://support.bind.pt/hc/en-us/articles/360015525971-AD-support-with-BindTuning-web-parts-" target="_blank">this article</a>.</p>

___
### Status

If the value is set to active, the link will be displayed on the quick links web part. Otherwise it will remain hidden.



After setting everything up, click on **Save** or **Save and create another**.

![save](../images/classic/16.save.png)


<p class="alert alert-success">Clicking <b>Save and create another</b> will keep the form open, so you can add more links to your page without closing the form.</p>
