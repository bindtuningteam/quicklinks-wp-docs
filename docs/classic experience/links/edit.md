1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part Links and click the **Manage Links** icon;
3. The list of Links will appear. Click the **Edit** icon to edit the Alerts;
4. You can check what you can edit in each section on the [Link Settings](../../global/link);

	![edit_linK](../../images/classic/04.edit_link.gif)

5. Done editing? Click on **Save Changes** to save your settings.

	![save_changes](../../images/classic/13.save_changes.png)