1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Links** icon;
3. The list of Alerts will appear. Click the **trash** icon to delete the Alerts;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Alerts will be removed.	

![delete_link](../../images/classic/03.delete_link.gif)