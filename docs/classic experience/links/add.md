1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new Link;

	![add_link](../../images/classic/02.add_link.gif)

4. Fill out the form that pops up. You can check what you need to do in each section on the [Link Settings](../../global/link);

5. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more alerts with similar configuration. 

	![save](../../images/classic/16.save.png)
