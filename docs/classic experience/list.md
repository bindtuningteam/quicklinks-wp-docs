### Create List

1. Open the settings menu and click on **Add an app**;

	![addanapp.png](../images/classic/17.addanapp.png)

2. On the search box type **BT**;
3. Look for the Links list and open it;
4. Pick a name for the app and click on **Create**;

	![linkslist.png](../images/classic/18.linkslist.png)

5. You've created the list that will contain all the specific fields from this web part.

6. Open the list that you have created, and copy the list URL, you will need it to connect the list to the web part.

![listlink.PNG](../images/classic/19.listlink.png)

____
### Name

If you want to give a name to your **Quick Links**, here is the place where you can define it.

___
### List URL

In this field, you need to paste the list URL that you copied when you finished creating the list.


<p class="alert alert-info">It is recommended that you use relative paths in this field.. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home/Lists/Links</b>, you should use something like <b>/sites/Home/Lists/Links</b>. This will ensure that the web part will work regardless of how you’re accessing the site.</p>

![list](../images/classic/07.list.png)